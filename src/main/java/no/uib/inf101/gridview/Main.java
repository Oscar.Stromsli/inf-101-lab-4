package no.uib.inf101.gridview;

import javax.swing.JFrame;
import java.awt.Color;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;


public class Main {
  public static void main(String[] args) {

    IColorGrid grid = new ColorGrid(3, 4);

    grid.set(new CellPosition(0, 0), Color.RED);
    grid.set(new CellPosition(0, 3), Color.BLUE);
    grid.set(new CellPosition(2, 0), Color.YELLOW);
    grid.set(new CellPosition(2, 3), Color.GREEN);

    System.out.println(grid.get(new CellPosition(0, 0))); 
    System.out.println(grid.get(new CellPosition(0, 3)));
    System.out.println(grid.get(new CellPosition(2, 0)));
    System.out.println(grid.get(new CellPosition(2, 3)));



    GridView canvas = new GridView(grid);
    JFrame frame = new JFrame();
    frame.setContentPane(canvas);
    frame.setTitle("LAB_4");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
  }
}
